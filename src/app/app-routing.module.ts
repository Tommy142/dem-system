import { NgModule } from '@angular/core';
import { LoginComponent } from './login';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeListComponent } from './employee-list';
import { noAuthGuardGuard } from './core/guards/no-auth-guard.guard';
import { authGuardGuard } from './core/guards/auth-guard.guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [noAuthGuardGuard],
  },
  {
    path: '',
    redirectTo: 'employee-list',
    pathMatch: 'full'
  },
  {
    path: 'employee-list',
    loadChildren: () => import('./employee-list/employee-list.module').then((m) => m.EmployeeListModule),
    canActivate: [authGuardGuard],
  },
  {
    path: '**',
    redirectTo: 'employee-list'
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
