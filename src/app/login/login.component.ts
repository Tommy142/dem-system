import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../core/services';
import { AlertDialogComponent } from '../core';
import { MatDialog, MatDialogConfig  } from '@angular/material/dialog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });
  errors: Error | null = null;
  isSubmitting = false;
  errorMessages = {
    required: 'This field is required.',
  };

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private dialog: MatDialog,
  ) {
  }
  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  getErrorMessage(controlName: string): string | null {
    const control = this.loginForm.get(controlName);

    if (control?.hasError('required')) {
      return this.errorMessages.required;
    }
    return null;
  }

  get form(): { [key:string]: AbstractControl } {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.isSubmitting = true
    if ( this.loginForm.invalid) {
      return;
    }

    const username = this.loginForm.controls['username'].value;
    const password = this.loginForm.controls['password'].value;

    const loginSuccessful = this.authService.login(username, password);

    if (loginSuccessful) {
      this.showAlert('Success', 'Login successful!', 'success');
      this.isSubmitting = false
      this.router.navigateByUrl('employee-list');
    } else {
      this.showAlert('Error', 'Invalid credentials!', 'error');
      this.isSubmitting = false
    }

  }

  showAlert(title: string, message: string, dialogType: 'success' | 'error' | 'warning'): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '300px';
    dialogConfig.data = { title, message, dialogType };

    const dialogRef = this.dialog.open(AlertDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


  onReset(): void {
    this.isSubmitting = false;
    this.loginForm.reset();
  }

}
