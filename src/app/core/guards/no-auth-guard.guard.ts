import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';

export const noAuthGuardGuard: CanActivateFn = (route, state) => {
  const token = localStorage.getItem('token');
  const router: Router = inject(Router)
  if (token) {
    return router.createUrlTree(['employee-list']);
  } else {
    return true;
  }
};
