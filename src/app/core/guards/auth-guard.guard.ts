import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';

export const authGuardGuard: CanActivateFn = (route, state) => {
  const token = localStorage.getItem('token');
  const router: Router = inject(Router)
  console.log('token', token)
  if (token) {
    return true;
  } else {
    return router.createUrlTree(['login']);
  }
};