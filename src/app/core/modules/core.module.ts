import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService, TokenService, EmployeeService } from '../services';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    TokenService,
    AuthService,
    EmployeeService
  ]
})
export class CoreModule { }
