import { Injectable } from '@angular/core';
import { TokenService } from './token.service';
import { Token } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  correctUsername = 'admin';
  correctPassword = 'password';

  constructor(private tokenService: TokenService) { }

  login(username: string, password: string): boolean {
    if (username === this.correctUsername && password === this.correctPassword) {
      let token: Token = this.tokenService.generateRandomToken()
      this.setAuth(token)
      return true;
    } else {
      return false;
    }
  }

  setAuth(token: Token) {
    this.tokenService.saveToken(token.token)
  }

  purgeAuth() {
    this.tokenService.destroyToken()
  }
}
