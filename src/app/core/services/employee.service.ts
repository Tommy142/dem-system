import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, tap, of } from 'rxjs';
import { Employee } from '../interfaces';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private readonly apiUrl = 'assets/data/employees.json';

  constructor(private http: HttpClient) { }

  getEmployees(): Observable<Employee[]> {
    const employees = localStorage.getItem('employees');

    if (employees) {
      return of(JSON.parse(employees));
    } else {
      return this.http.get<Employee[]>(this.apiUrl).pipe(
        tap(data => {
          console.log('data', data)
          localStorage.setItem('employees', JSON.stringify(data));
          return data;
        })
      );
    }
  }

  storeEmployee(employee: Employee) {
    let employees = [];
    const item = localStorage.getItem('employees');
    if (item !== null) {
      employees = JSON.parse(item);
    }
    employees.push(employee);
    localStorage.setItem('employees', JSON.stringify(employees));
  }

  getEmployeeByEmail(email: string): Employee | null {
    const employees = localStorage.getItem('employees');

    if (employees) {
      const employeesArray: Employee[] = JSON.parse(employees);

      const employee = employeesArray.find(emp => emp.email === email);

      return employee ? employee : null;
    } else {
      return null;
    }
  }
}
