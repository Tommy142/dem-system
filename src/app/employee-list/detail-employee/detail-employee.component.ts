import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/core';

@Component({
  selector: 'app-detail-employee',
  templateUrl: './detail-employee.component.html',
  styleUrls: ['./detail-employee.component.css']
})
export class DetailEmployeeComponent {
  employeeForm: FormGroup;
  ;
  constructor(private route: ActivatedRoute, private employeeService: EmployeeService, private router: Router) {
    this.employeeForm = new FormGroup({
      username: new FormControl(''),
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      email: new FormControl(''),
      birthDate: new FormControl(''),
      basicSalary: new FormControl(''),
      status: new FormControl(''),
      group: new FormControl(''),
      description: new FormControl('')
    });
  }

  ngOnInit() {
    const email = this.route.snapshot.paramMap.get('email');
    const employee = this.employeeService.getEmployeeByEmail(email!)!;
    const formattedSalary = new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(employee.basicSalary);

    this.employeeForm.setValue({
      username: employee.username,
      firstName: employee.firstName,
      lastName: employee.lastName,
      email: employee.email,
      birthDate: employee.birthDate,
      basicSalary: formattedSalary,
      status: employee.status,
      group: employee.group,
      description: employee.description
    });

  }

  onCancel() {
    this.router.navigateByUrl('employee-list');
  }

}
