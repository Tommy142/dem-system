import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Employee, EmployeeService } from 'src/app/core';
import { AlertDialogComponent } from '../../core';
import { MatDialog, MatDialogConfig  } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  employeeForm: FormGroup = new FormGroup({});
  today = new Date();
  groups = ['Group 1', 'Group 2', 'Group 3', 'Group 4', 'Group 5', 'Group 6', 'Group 7', 'Group 8', 'Group 9', 'Group 10'];
  isSubmitting: boolean = false;
  statuses = ['Active', 'Inactive'];
  errorMessages = {
    required: 'This field is required.',
    email: 'Please enter a valid email address.',
    number: 'Must be a number.',
  };

  constructor(
    private fb: FormBuilder,
    private employeeService: EmployeeService,
    private router: Router,
    private dialog: MatDialog,

  ) {}

  ngOnInit() {
    this.employeeForm = this.fb.group({
      'username': ['', Validators.required],
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'email': ['', [Validators.required, Validators.email]],
      'birthDate': ['', Validators.required],
      'basicSalary': ['', Validators.required],
      'status': ['', Validators.required],
      'group': ['', Validators.required],
      'description': ['', Validators.required]
    });
  }

  getErrorMessage(controlName: string): string | null {
    const control = this.employeeForm.get(controlName);

    if (control?.hasError('required')) {
      return this.errorMessages.required;
    }

    if (control?.hasError('email')) {
      return this.errorMessages.email;
    }

    return null;
  }

  get form(): { [key:string]: AbstractControl } {
    return this.employeeForm.controls;
  }

  onSubmit() {
    this.isSubmitting = true;
    if (this.employeeForm.invalid) {
      return;
    }
    let employee: Employee = {
      firstName: this.employeeForm.controls['firstName'].value,
      lastName: this.employeeForm.controls['lastName'].value,
      email: this.employeeForm.controls['email'].value,
      birthDate: this.employeeForm.controls['birthDate'].value,
      basicSalary: this.employeeForm.controls['basicSalary'].value,
      group: this.employeeForm.controls['group'].value,
      description: this.employeeForm.controls['description'].value,
      status: this.employeeForm.controls['status'].value,
      username: this.employeeForm.controls['username'].value,

    }
    this.employeeService.storeEmployee(employee)
    this.showAlert('Success', 'Add Employee successful!', 'success');
    this.isSubmitting = false
    this.router.navigateByUrl('employee-list');
  }

  onCancel() {
    this.router.navigateByUrl('employee-list');
  }

  dateFilter = (date: Date | null): boolean => {
    const current = new Date();
    return !!date && date <= current;
  }

  onlyDecimalNumberKey(event: any) {
    let charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;
    return true;
  }

  showAlert(title: string, message: string, dialogType: 'success' | 'error' | 'warning'): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '300px';
    dialogConfig.data = { title, message, dialogType };

    const dialogRef = this.dialog.open(AlertDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
