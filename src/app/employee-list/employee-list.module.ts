import { NgModule } from '@angular/core';
import { SharedModule } from '../core';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { EmployeeListRoutingModule } from './employee-list-routing.module';
import { EmployeeListComponent } from './employee-list.component';
import { DetailEmployeeComponent } from './detail-employee/detail-employee.component';



@NgModule({
  declarations: [EmployeeListComponent, AddEmployeeComponent, DetailEmployeeComponent],
  imports: [
    SharedModule,
    EmployeeListRoutingModule,
  ]
})
export class EmployeeListModule { }
