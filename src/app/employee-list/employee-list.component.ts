import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Employee } from '../core';
import { EmployeeService } from '../core/services';
import { ChangeDetectorRef } from '@angular/core';
import { AlertDialogComponent } from '../core';
import { MatDialog, MatDialogConfig  } from '@angular/material/dialog';
import { Router } from '@angular/router';


interface ColumnKeys {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: string;
  status: string;
  group: string;
  description: string;
}

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})

export class EmployeeListComponent implements AfterViewInit {
  dataSource = new MatTableDataSource<Employee>();
  displayedColumns: string[] = ['username', 'firstName', 'lastName', 'email', 'birthDate', 'basicSalary', 'status', 'group', 'description'];
  allColumns: string[] = ['index', ...this.displayedColumns, 'actions'];
  filterValues = {
    firstName: '',
    lastName: '',
    email: ''
  };

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private employeeService: EmployeeService,
    private dialog: MatDialog,
    private router: Router,
  ) {}

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.employeeService.getEmployees().subscribe(data => {
      this.dataSource.data = data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.dataSource.filterPredicate = (data: Employee, filter: string): boolean => {
      const searchTerms = JSON.parse(filter);
      return data.firstName.toLowerCase().includes(searchTerms.firstName)
        && data.lastName.toLowerCase().includes(searchTerms.lastName)
        && data.email.toLowerCase().includes(searchTerms.email);
    };

  }

  applyFilter(event: Event, column: string): void {
    const value = (event.target as HTMLInputElement).value;
    this.filterValues[column as keyof typeof this.filterValues] = value;
    this.dataSource.filter = JSON.stringify(this.filterValues)

    // localStorage.setItem(column, value);
  }

  getDisplayValue(column: string): any {
    const keys: ColumnKeys = {
      username: 'Username',
      firstName: 'First Name',
      lastName: 'Last Name',
      email: 'E-mail',
      birthDate: 'Birth Date',
      basicSalary: 'Basic Salary',
      status: 'Status',
      group: 'Group',
      description: 'Description'
    };

    if (keys.hasOwnProperty(column)) {
      return keys[column as keyof ColumnKeys];
    } else {
      return 'Unknown Column';
    }
  }

  detailEmployee(row: Employee){
    this.router.navigate(['employee-list/detail-employee', row.email]);
  }


  editEmployee(row: Employee) {
    this.showAlert('Edit Employee', 'Edit Employee has been successful', 'warning')
  }

  deleteEmployee(row: Employee) {
    this.showAlert('Delete Employee', 'Delete Employee has been successful', 'error')
  }

  showAlert(title: string, message: string, dialogType: 'success' | 'error' | 'warning'): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '300px';
    dialogConfig.data = { title, message, dialogType };

    const dialogRef = this.dialog.open(AlertDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getIndex(index: number): number {
    return this.paginator.pageSize * this.paginator.pageIndex + index + 1;
  }

  getLocalStorageItem(key: string): string | null {
    return localStorage.getItem(key);
  }
}
