# DEM-System

This is an Angular project with login, employee-list, add-employee, edit and delete employee functionalities.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them:

- Node.js version 18.18.0
- Angular CLI

### Installing

A step by step series of examples that tell you how to get a development environment running:

1. Clone the repo: `git clone https://gitlab.com/Tommy142/dem-system.git`
2. Go into the repo: `cd dem-system`
3. Install dependencies: `npm install`
4. Start the server: `ng serve`

Now navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Built With

- Angular - The web framework used

## Information
    username: admin
    password: password

## Authors

- Tommy - Initial work - Tommy142
    - Set up the initial project structure using Angular CLI.
    - Implemented core features such as login, employee-list, add-employee, edit and    delete employee functionalities.
    - Designed the user interface using Angular Material.
    - Wrote the initial documentation and README.md file

## License

This project is licensed under the MIT License - see the LICENSE.md file for details
